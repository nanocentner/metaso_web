$(document).ready(function(){	
	$("#addtask").click(function( event ){
		event.preventDefault();
		$("#newtaskform").fadeToggle("fast");
		
		if($("#send").css("display") == "none" )
			$("#send").fadeToggle("fast");
		
	  });

	$("#cancel").click(function(){
		event.preventDefault();
		$("#newtaskform").fadeToggle("fast");
	});

	$(document).keyup(function(e) {
		if(e.keyCode == 27 && $("#newtaskform").css("display") != "none"  && $("#send").css("display") != "none" ) { 
			event.preventDefault();
			$("#newtaskform").fadeToggle("fast");
		}
	});
	
	$("#send").click(function(){
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: "/php/task_mng.php",
			data: { 
				action: 'add_task',
				task: $("#task").val(), 
			},
			success: function(result) {
				var res = result.split(":");
				if(res[0] == "-1")
				{
					alert(res[1]);
				}
				else
				{
					window.location.reload(false);
				}
			},
			error: function(result) {
				alert('error');
			}
		});
	});
	
});

// Handler for reject button
function RejectTask(id) {
	$.ajax({
		type: "POST",
		url: "/php/task_mng.php",
		data: { 
			action: 'reject_task',
			task: id,
			comment: $("#task").val(),	
		},
		success: function(result) {
			var res = result.split(":");
			if(res[0] == "-1")
			{
				alert(res[1]);
			}
			else
			{
				window.location.reload(false);
			}
		},
		error: function(result) {
			alert('error');
		}
	});
}

// Handler for confirm button
function ConfirmTask(id) {
	$.ajax({
		type: "POST",
		url: "/php/task_mng.php",
		data: { 
			action: 'confirm_task',
			task: id,
			comment: $("#task").val(),			
		},
		success: function(result) {
			var res = result.split(":");
			if(res[0] == "-1")
			{
				alert(res[1]);
			}
			else
			{
				window.location.reload(false);
			}
		},
		error: function(result) {
			alert('error');
		}
	});
}

// Add comment
function AddComment(id) {
	$("#newtaskform").fadeToggle("fast");
	
	if($("#send").css("display") != "none" )
		$("#send").fadeToggle("fast");
}