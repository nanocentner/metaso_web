function parse_sensor(xml_raw_data, tableId) {
    sensors=[];
    $(xml_raw_data).find('sensors > add').each(function(){
        sensors.push(($(this).attr('label')).concat("=", $(this).attr('value'), $(this).attr('sufix')));
    });
    if (sensors.length === 0) {return}
    fill_vert_table(tableId, sensors);    
};

function parse_system(xml_raw_data, tableId) {
    sensors=[];
    $(xml_raw_data).find('system > add').each(function(){
        sensors.push(($(this).attr('label')).concat("=", $(this).attr('value')));
    });
    if (sensors.length === 0) {return}
    fill_hor_table(tableId, sensors);    
};

function parse_trigger(xml_raw_data, tableId) {
    sensors=[];
    $(xml_raw_data).find('trigger > add').each(function(){
        sensors.push(($(this).attr('label')).concat("=", $(this).attr('value')));
    });
    if (sensors.length === 0) {return}
    fill_hor_table(tableId, sensors);    
};

function parse_pulse(xml_raw_data, tableId) {
    sensors=[];
    $(xml_raw_data).find('pulse > add').each(function(){
        sensors.push(($(this).attr('label')).concat("=", $(this).attr('value')));
    });
    if (sensors.length === 0) {return}
    fill_hor_table(tableId, sensors);    
};

function parse_io(xml_raw_data, tableId) {
    var tableBody = "";
    
    if($(xml_raw_data).find('io').children().length === 0)
        return;
    
    $(tableId+" tr").remove();
    
    tableBody = tableBody + "<tr>"
    tableBody = tableBody + ("<th>Pin</th><th>Mode</th><th>Direction</th><th>Value</th>");
    tableBody = tableBody + "</tr>"
    
    $(xml_raw_data).find('io > add').each(function(){
        if($(this).attr('mode'))
        tableBody = tableBody + "<tr>"
	    tableBody = tableBody + ("<td>" + $(this).attr('label') + "</td>");
        tableBody = tableBody + ("<td>" + $(this).attr('mode') + "</td>");
        if($(this).attr('direction') !== undefined) {
            tableBody = tableBody + ("<td>" + $(this).attr('direction') + "</td>");
        } else {
            tableBody = tableBody + ("<td> -</td>");
        }
        if($(this).attr('value') !== undefined) {
            tableBody = tableBody + ("<td>" + $(this).attr('value') + "</td>");
        } else {
            tableBody = tableBody + ("<td> -</td>");
        }
	    tableBody = tableBody + "</tr>"
    });
    $(tableId).append(tableBody);
};

function parse_uart(xml_raw_data, tableId) {
    sensors=[];
    $(xml_raw_data).find('uart > add').each(function(){
        sensors.push(($(this).attr('label')).concat("=", $(this).attr('value')));
    });
    if (sensors.length === 0) {return}
    fill_hor_table(tableId, sensors);  
};

function fill_vert_table(id, array) {	
	 var tableBody = "";
	 
	 $(id+" tr").remove();
	 
	 for (item of array) {
        if (item === "")
            continue;
        
        sublist=item.split("="); 
	    tableBody = tableBody + "<tr>"
	    tableBody = tableBody + ("<th>" + sublist[0] + "</th>");
	    tableBody = tableBody + ("<td>" + sublist[1] + "</td>");
	    tableBody = tableBody + "</tr>"
	}
		
	$(id).append(tableBody);
};

function fill_hor_table(id, array) {	
    var tableBody = "";

    $(id+" tr").remove();
    label = []
    value = []

    for (item of array) {
        if (item === "")
            continue;

        sublist=item.split("="); 

        label.push(sublist[0])
        value.push(sublist[1])
    }
    
    tableBody = tableBody + "<tr>"
    for (item of label) {
        tableBody = tableBody + ("<th>" + item + "</th>");
    }
    tableBody = tableBody + "</tr>"  
	
    tableBody = tableBody + "<tr>"
    for (item of value) {
        tableBody = tableBody + ("<td>" + item + "</td>");
    }
    tableBody = tableBody + "</tr>"  
	
	$(id).append(tableBody);
};

function parse_log(xml_raw_data, tableId) {
    var tableBody = "";
       
    $(tableId+" tr").remove();
    
    tableBody = tableBody + "<tr>"
    tableBody = tableBody + ("<th>Timestamp</th>")
    tableBody = tableBody + ("<th>Elapsed Time</th>")
    tableBody = tableBody + ("<th>Trig slope</th>")
    tableBody = tableBody + ("<th>Trig edge count</th>");
    tableBody = tableBody + ("<th>Pulse delay (ns)</th>");
    tableBody = tableBody + ("<th>Pulse length (ns)</th>");
    tableBody = tableBody + ("<th>DUT themperature</th>");
    tableBody = tableBody + ("<th>DUT state</th>");
    tableBody = tableBody + ("<th>Comment</th>");
    tableBody = tableBody + "</tr>"
    
       
    $(xml_raw_data).find('log').each(function(){    
        var temp;
        tableBody = tableBody + "<tr>"
        temp = typeof $(this).attr('timestamp') !== 'undefined' ? $(this).attr('timestamp') : '-';
	    tableBody = tableBody + ("<td>" + temp + "</td>");
        temp = typeof $(this).attr('elapsedtime') !== 'undefined' ? $(this).attr('elapsedtime') : '-';
	    tableBody = tableBody + ("<td>" + temp + "</td>");
        temp = typeof $(this).attr('trig_slope') !== 'undefined' ? $(this).attr('trig_slope') : '-';
        tableBody = tableBody + ("<td>" + temp + "</td>");
        temp = typeof $(this).attr('trig_edge_cnt') !== 'undefined' ? $(this).attr('trig_edge_cnt') : '-';
        tableBody = tableBody + ("<td>" + temp + "</td>");
        temp = typeof $(this).attr('pulse_delay_ns') !== 'undefined' ? $(this).attr('pulse_delay_ns') : '-';
        tableBody = tableBody + ("<td>" + temp + "</td>");
        temp = typeof $(this).attr('pulse_length_ns') !== 'undefined' ? $(this).attr('pulse_length_ns') : '-';
        tableBody = tableBody + ("<td>" + temp + "</td>");
        temp = typeof $(this).attr('dut_therm_c') !== 'undefined' ? $(this).attr('dut_therm_c') : '-';
        tableBody = tableBody + ("<td>" + temp + "</td>");
        temp = typeof $(this).attr('dut_state') !== 'undefined' ? $(this).attr('dut_state') : '-';
        tableBody = tableBody + ("<td>" + temp + "</td>");
        
        temp = typeof $(this).text() !== 'undefined' ? $(this).text() : '-';
        tableBody = tableBody + ("<td>" + temp + "</td>");
	    tableBody = tableBody + "</tr>"
    });
    $(tableId).append(tableBody);
};

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
    }
    return "";
}