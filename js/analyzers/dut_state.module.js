/*!
 *  Project: DUT state plotter
 */

var plot_config = {
    "type": "range",
	"scroll-x": {
	},
	"scroll-y": {
	},
	"scale-x": {
		"zooming": true,
		"item": {
		  "font-size": 10
		}
	},
	"scale-y": {
		"zooming": true,
		"guide": {
		  "line-style": "dotted"
		},
		"item": {
		  "font-size": 10
		}
	},
	"plot": {
		//'aspect' : "spline",
		"line-width": 1,
		"line-color": "#3399ff",
		"marker": {
		  "size": 3,
		  "background-color": "#ccccff #00ff00",
		  "border-width": 1,
		  "border-color": "#3399ff"
		},
		"tooltip": {
		  "visible": false
		}
	},
	"crosshair-x": {
		'plotLabel':
		{
			'multiple': true,
		},
		"marker": {
		  "size": 4,
		  "type": "circle",
		  "borderColor": "#fff",
		  "borderWidth": 1
		},
		"scale-label": {
		  //"visible": false
		}
	},
	"crosshair-y": {
		//"type": "multiple",
		"scale-label": {
		  "visible": false
		}          
	},
	"series": Array()
    };

/*!
 *  Plot graphics
 */  
function plot_graph(plotter_id, plot_data=Array()) {
    
    plot_config["scale-x"]["labels"] = plot_data.labels
    
    plot_config["series"] = Array(); // Clear previous    
    for(let i=0; i<plot_data.series.length; i++) {
		plot_config["series"].push({"values": plot_data.series[i], 'text': plot_data.series_label[i],})
	}	
		
	zingchart.render({
		id: plotter_id,
		data: plot_config,
		height: 400,
		width: "100%"
	});
}

/*!
 *  Process data
 */
function process_data(view_id, list_item=Array()) {
	const state_def = ['normal', 'reset', 'success', 'adnormal']
	let plot_data = {labels:[], series: [[], [], [], []], series_label:["Normal", "Reset" , "Success", "Adnormal"]}
	let groups=[];
		
	if(list_item.length == 0)
		return;
	
	// First scan array, to get sorted delay parameters
	list_item.forEach(function(item){
		if(state_def.indexOf(item.dut_state) == -1)
			return;
			
		let index = plot_data.labels.indexOf(item.pulse_delay_ns)
		if(index == -1) plot_data.labels.push(item.pulse_delay_ns)
	});	
	
	// Sort delay and init series
	plot_data.labels.sort(function(a, b) {
		return a-b;
	})
	
	for(let i=0; i<plot_data.series.length; i++) {
		plot_data.series[i] = Array.from({length:plot_data.labels.length}, ()=>(null))
	}
	
	// Sort length by delay
	list_item.forEach(function(item){
		let graph_index = state_def.indexOf(item.dut_state);
		
		if(graph_index == -1)
			return;
		
		if(graph_index == 0)
			console.log()
					
		let index = plot_data.labels.indexOf(item.pulse_delay_ns)
		
		//console.log(item.dut_state + " delay: " + item.pulse_delay_ns + " length: " + item.pulse_length_ns)
		if(plot_data.series[graph_index][index] == null) {
			plot_data.series[graph_index][index] = [item.pulse_length_ns, item.pulse_length_ns]
		}
		else {
			if(item.pulse_length_ns > plot_data.series[graph_index][index][1])
				plot_data.series[graph_index][index][1] = item.pulse_length_ns
			
			if(item.pulse_length_ns < plot_data.series[graph_index][index][0])
				plot_data.series[graph_index][index][0] = item.pulse_length_ns 
		}
		
			
	});
	
	plot_graph(view_id, plot_data);
}

export { process_data};
