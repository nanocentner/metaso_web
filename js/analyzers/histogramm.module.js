/*!
 *  Project: Histogramm plotter
 */

var plot_config = {
	"type": "bar",
	"scroll-x": {
	},
	"scroll-y": {
	},
	"scale-x": {
		"zooming": true,
		"item": {
		  "font-size": 10
		}
	},
	"scale-y": {
		"zooming": true,
		"guide": {
		  "line-style": "dotted"
		},
		"item": {
		  "font-size": 10
		}
	},
	"plot": {
		'stacked': true,
		"line-width": 1,
		"line-color": "#3399ff",
		"marker": {
		  "size": 3,
		  "background-color": "#ccccff #00ff00",
		  "border-width": 1,
		  "border-color": "#3399ff"
		},
		"tooltip": {
		  "visible": false
		}
	},
	"crosshair-x": {
		'plotLabel':
		{
			'multiple': true,
		},
		"marker": {
		  "size": 4,
		  "type": "circle",
		  "borderColor": "#fff",
		  "borderWidth": 1
		},
		"scale-label": {
		  //"visible": false
		}
	},
	"crosshair-y": {
		//"type": "multiple",
		"scale-label": {
		  "visible": false
		}          
	},
	"series": Array()
};

/*!
 *  Plot graphics
 */  
function plot_graph(plotter_id, plot_data=Array()) {
    
    plot_config["scale-x"]["labels"] = plot_data.labels
    plot_config["series"] = Array(); // Clear previous    
       
    for(let i=0; i<plot_data.series.length; i++) {
		plot_config["series"].push({"values": plot_data.series[i], 'text': plot_data.series_label[i],})
	}	
		
	zingchart.render({
		id: plotter_id,
		data: plot_config,
		height: 400,
		width: "100%"
	});
}

/*!
 *  Process data
 */
function process_data(view_id, list_item=Array()) {
	let plot_data = {labels:[], series: [ [], []], series_label:["Total", "Success"]}
	let groups=[];
		
	if(list_item.length == 0)
		return;
		
	/* Sort by group */
	list_item.forEach(function(item){	
		let curKey = item.pulse_length_ns + ":" + item.pulse_delay_ns
		
		let index = groups.map(function(e) {return e.key;}).indexOf(curKey)
		if(index != -1) {
			groups[index].item_list.push(item);	
		}
		else
		{
			groups.push({key:curKey, item_list:Array()});
			groups[groups.length-1].item_list.push(item);
		}		
	});
	
	//appendLogMsg("Sort " + logListItem.length + " items into " + groups.length + " group");
	
	/* Launch analyze of groups */
		
	// Get success procents
	groups.forEach(function(item) {
		var sucCount = 0;
		item.item_list.forEach(function(it){
			if(it.dut_state == "success") sucCount=sucCount+1;
		});
		
		plot_data.labels.push(item.key);
		plot_data.series[0].push(item.item_list.length);
		plot_data.series[1].push(sucCount);
		//appendLogMsg(item.key + " Try: " + item.item_list.length + " Success: " + sucCount + " " + ((sucCount*100)/item.item_list.length).toFixed(2) + "%");
	
	});
	
	plot_graph(view_id, plot_data);
}

export { process_data};
