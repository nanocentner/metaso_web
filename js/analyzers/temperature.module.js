/*!
 *  Project: Temperature plotter
 */

var plot_config = {
      "type": "line",
      "scroll-x": {
 
      },
      "scroll-y": {
 
      },
      "scale-x": {
        "zooming": true,
        "item": {
          "font-size": 10
        }
      },
      "scale-y": {
        "zooming": true,
        "guide": {
          "line-style": "dotted"
        },
        "item": {
          "font-size": 10
        }
      },
      "plot": {
        "line-width": 1,
        "line-color": "#3399ff",
        "marker": {
          "size": 3,
          "background-color": "#ccccff #00ff00",
          "border-width": 1,
          "border-color": "#3399ff"
        },
        "tooltip": {
          "visible": false
        }
      },
      "crosshair-x": {
        "plot-label": {
          'multiple': true,
        },
        "scale-label": {
        }
      },
      "crosshair-y": {
        "type": "multiple",
        "scale-label": {
          "visible": false
        }
      },
      "series": Array()
    };

/*!
 *  Plot graphics
 */  
function plot_graph(plotter_id, plot_data=Array()) {
    
    plot_config["scale-x"]["labels"] = plot_data.labels
    
    plot_config["series"] = Array(); // Clear previous     
    for(let i=0; i<plot_data.series.length; i++) {
		plot_config["series"].push({"values": plot_data.series[i], 'text': plot_data.series_label[i],})
	}	
		
	zingchart.render({
		id: plotter_id,
		data: plot_config,
		height: 400,
		width: "100%"
	});
}

/*!
 *  Process data
 */
function process_data(view_id, list_item=Array()) {
	let plot_data = {labels:[], series: [ [] ], series_label:["T"]}
	let groups=[];
		
	if(list_item.length == 0)
		return;
		
	list_item.forEach(function(item){
		if(!isNaN(item.dut_therm_c)) {		
			groups.push({key:String(item.date), item_list:Array()});
			groups[groups.length-1].item_list.push(item);
		}
		
	});
	
	// Get success procents
	groups.forEach(function(item) {		
		plot_data.labels.push(item.key);
		plot_data.series[0].push(item.item_list[0].dut_therm_c);
	});
	
	plot_graph(view_id, plot_data);
}

export { process_data};
