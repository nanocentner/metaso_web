export function logitem() {
    this.date = null;
    this.elapsedtime = null;
    this.pulse_length_ns = null;
    this.pulse_delay_ns = null;
    this.dut_therm_c = null;
    this.trig_edge_cnt= null;
    this.trig_slope=null;
    this.comment = null;
    this.dut_state = null;
}


export function parseLog2List(xmlStrings)
{
	let logList=[];
			
	logList.push(new logitem() );
	logList[0].pulse_length_ns = 10;
	logList[0].pulse_delay_ns = 100;
	return logList;	
}

