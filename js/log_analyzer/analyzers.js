function logitem() {
    this.date = null;
    this.elapsedtime = null;
    this.pulse_length_ns = null;
    this.pulse_delay_ns = null;
    this.dut_therm_c = null;
    this.trig_edge_cnt= null;
    this.trig_slope=null;
    this.comment = null;
    this.dut_state = null;
}

// List of avaible analysis and description of them
var anllist = [];

anllist.push({
label: "Histogram", 
description: "Get procent of success, and average time for data extraction",
clbkf: function(logListItem){histo_cbf(logListItem)}
})

anllist.push({
label: "Dut states", 
description: "Get graphics of DUT states over pulse delay and length",
clbkf: function(logListItem){dut_state_cbf(logListItem)}
})


anllist.push({
label: "Temperature", 
description: "Simple graphics of temperature changes",
clbkf: function(logListItem){temp_cbf(logListItem)}
})



var groups=[];

/*!
 *  Append message into web page
 */
function appendLogMsg(msg) {
	logDivObj=document.getElementById("anallog");
	logDivObj.innerHTML= logDivObj.innerHTML+"<p>" + msg + "</p>";
} 

/*!
 *  Clear message into web page
 */
function cleardLogMsg() {
	logDivObj=document.getElementById("anallog");
	logDivObj.innerHTML= "";
} 

/*!
 *  Plote bars base on data
 */
function plotBars(data) {
	var myConfig = {
		"type": "bar",
		"scroll-x": {

		},
		"scroll-y": {

		},
		"scale-x": {
			"zooming": true,
			labels: data.labels,
			"item": {
			  "font-size": 10
			}
		},
		"scale-y": {
			"zooming": true,
			//"values": "50:350:50",
			"guide": {
			  "line-style": "dotted"
			},
			"item": {
			  "font-size": 10
			}
		},
		"plot": {
			'stacked': true,
			"line-width": 1,
			"line-color": "#3399ff",
			"marker": {
			  "size": 3,
			  "background-color": "#ccccff #00ff00",
			  "border-width": 1,
			  "border-color": "#3399ff"
			},
			"tooltip": {
			  "visible": false
			}
		},
		"crosshair-x": {
			'plotLabel':
			{
				'multiple': true,
			},
			"marker": {
			  "size": 4,
			  "type": "circle",
			  "borderColor": "#fff",
			  "borderWidth": 1
			},
			"scale-label": {
			  //"visible": false
			}
		},
		"crosshair-y": {
			//"type": "multiple",
			"scale-label": {
			  "visible": false
			}          
		},
		"series": Array()
	};
  
  
  		
	for(var i=0; i<data.series.length; i++) {
		myConfig["series"].push({
			'values': data.series[i], 
			'text': data.series_label[i],
			});
	}	
 	
 		
	zingchart.render({
		id: 'chart01',
		data: myConfig,
		height: 400,
		width: "100%"
	});
    
}


/*!
 *  Plote line base on data
 */
function plotLines(data) {

var myConfig = {
      "type": "line",
      "scroll-x": {
 
      },
      "scroll-y": {
 
      },
      "scale-x": {
        "zooming": true,
        labels: data.labels,
        "item": {
          "font-size": 10
        }
      },
      "scale-y": {
        "zooming": true,
        //"values": "50:350:50",
        "guide": {
          "line-style": "dotted"
        },
        "item": {
          "font-size": 10
        }
      },
      "plot": {
        "line-width": 1,
        "line-color": "#3399ff",
        "marker": {
          "size": 3,
          "background-color": "#ccccff #00ff00",
          "border-width": 1,
          "border-color": "#3399ff"
        },
        "tooltip": {
          "visible": false
        }
      },
      "crosshair-x": {
        "plot-label": {
          'multiple': true,
        },
        "scale-label": {
        }
      },
      "crosshair-y": {
        "type": "multiple",
        "scale-label": {
          "visible": false
        }
      },
      "series": Array()
    };
 
    for(var i=0; i<data.series.length; i++) {
      myConfig["series"].push({"values": data.series[i], 'text': data.series_label[i],})
    }	
    		
    zingchart.render({
      id: 'chart01',
      data: myConfig,
      height: 400,
      width: "100%"
    });
}

/*!
 *  Parse raw string into log items. Prepare data for load into analizator modules.
 */
function parseLog2List(xmlStrings)
{
	var logList=[];
	var index = -1;
	xmlStrings = "<loglist>".concat(xmlStrings);
	xmlStrings = xmlStrings.concat("</loglist>");
	
	pxml = $.parseXML(xmlStrings)
	
	$(pxml).find('log').each(
		function(){
			logList.push(new logitem());
			index = index + 1;
			logList[index].date = $(this).attr("timestamp");
			logList[index].elapsedtime = parseFloat($(this).attr("elapsedtime"));
			logList[index].pulse_length_ns = parseInt($(this).attr("pulse_length_ns"));
			logList[index].pulse_delay_ns = parseInt($(this).attr("pulse_delay_ns"));
			logList[index].dut_therm_c = parseFloat($(this).attr("dut_therm_c"));
			logList[index].trig_edge_cnt = $(this).attr("trig_edge_cnt");
			logList[index].trig_slope = $(this).attr("trig_slope");
			logList[index].comment = $(this).text();
			logList[index].dut_state = $(this).attr("dut_state");
		});
		
	return logList;	
}

/*
 *  Histogram analizator
 */
function histo_cbf(logListItem=Array())
{
	groups=[];
	var plotdata = {labels:[], series: [ [], []], series_label:["Total", "Success"]}
		
	if(logListItem.length == 0)
		return;
	
	/* Sort by group */
	logListItem.forEach(function(item){	
			curKey = item.pulse_length_ns + ":" + item.pulse_delay_ns
			
			index = groups.map(function(e) {return e.key;}).indexOf(curKey)
			if(index != -1) {
				groups[index].item_list.push(item);	
			}
			else
			{
				groups.push({key:curKey, item_list:Array()});
				groups[groups.length-1].item_list.push(item);
			}		
	});
	
	appendLogMsg("Sort " + logListItem.length + " items into " + groups.length + " group");
	
	/* Launch analyze of groups */
		
	// Get success procents
	groups.forEach(function(item) {
		var sucCount = 0;
		item.item_list.forEach(function(it){
			if(it.dut_state == "success") sucCount=sucCount+1;
		});
		
		plotdata.labels.push(item.key);
		plotdata.series[0].push(item.item_list.length);
		plotdata.series[1].push(sucCount);
		appendLogMsg(item.key + " Try: " + item.item_list.length + " Success: " + sucCount + " " + ((sucCount*100)/item.item_list.length).toFixed(2) + "%");
	
	});
	
	plotBars(plotdata);
}

/*
 *  Dut state analizator
 */
function dut_state_cbf(logListItem=Array())
{
	groups=[];
	var plotdata = {labels:[], series: [[12], [13], [14], [15]], series_label:["Normal", "Reset" , "Success", "Adnormal"]}
	
	if(logListItem.length == 0)
		return;
	
	// First scan array, to get sorted delay parameters
	logListItem.forEach(function(item){
		var pulse_delay = 0
		if(item.dut_state == "success") pulse_delay = item.pulse_delay_ns
		else if(item.dut_state == "reset") pulse_delay = item.pulse_delay_ns
		else if(item.dut_state == "normal") pulse_delay = item.pulse_delay_ns
		else if(item.dut_state == "adnormal") pulse_delay = item.pulse_delay_ns
		else return;
		
		index = plotdata.labels.indexOf(pulse_delay)
		if(index == -1) plotdata.labels.push(pulse_delay)
		
	});
	
	plotdata.labels.
	
	plotLines(plotdata);
}


/*
 *  Temperature analizator
 */
function temp_cbf(logListItem=Array())
{
	groups=[];
	var plotdata = {labels:[], series: [[]], series_label:["T"]}
		
	if(logListItem.length == 0)
		return;
		
	logListItem.forEach(function(item){
		if(!isNaN(item.dut_therm_c)) {		
			curKey = String(item.date);
			groups.push({key:curKey, item_list:Array()});
			groups[groups.length-1].item_list.push(item);
		}
		
	});
	
	// Get success procents
	groups.forEach(function(item) {		
		plotdata.labels.push(item.key);
		plotdata.series[0].push(item.item_list[0].dut_therm_c);
	});
	
	plotLines(plotdata);	
}
