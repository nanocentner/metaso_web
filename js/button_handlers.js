function restart(){
	  $.ajax({
	    url: './cgi-bin/restart',
	    success: 
	      function(data){
	      	alert("System reboot now.")
            ajaxDisable = true;
            setTimeout(()=>location.reload(), 30000);
	    }
        });
    };
    
function poweroff(){
  $.ajax({
    url: './cgi-bin/poweroff',
    success: 
      function(data){
      	alert("System shutdown now")
    setTimeout(()=>alert("Close window, and power off hardware"), 10000);        
    }
});
};

function jupyter_launch(){
	var host = $(location).attr("host");
	$("#juplink").attr("href", "http://"+host+":8888");
};
