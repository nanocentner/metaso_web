#!/usr/bin/python3
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from pathlib import Path
import subprocess, os.path
from xml.etree import ElementTree
from xml.dom import minidom

METASO_STATE_XML='/tmp/metaso_state.xml'

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

print("Content-type: text/plain; charset=iso-8859-1\n")

top = Element('top')

comment = Comment('Generated for Metaso V4')
top.append(comment)

group = SubElement(top, 'sensors')
result = subprocess.run(["free | grep Mem | awk '{print int($3/$2*100)}'"],
                          shell=True, stdout=subprocess.PIPE)

child = SubElement(group, 'add',
                   {'label' : 'RAM',
                   'value' : result.stdout.decode('utf-8'),
                   'sufix' : '%'})

result = subprocess.run(["df --type=ext4 | xargs | cut -d' ' -f12"],
                          shell=True, stdout=subprocess.PIPE)
child = SubElement(group, 'add',
                   {'label' : 'FLASH',
                   'value' : result.stdout.decode('utf-8'),
                   'sufix' : ''})

result = subprocess.run(["cat /sys/class/hwmon/hwmon0/temp1_input | awk '{print int($1/1000)}'"],
                          shell=True, stdout=subprocess.PIPE)
child = SubElement(group, 'add',
                   {'label' : 'Temperature',
                   'value' : result.stdout.decode('utf-8'),
                   'sufix' : 'C'})

result = subprocess.run(["date +'%d %B %Y (%H:%M)'"],
                          shell=True, stdout=subprocess.PIPE)
child = SubElement(group, 'add',
                   {'label' : 'Date',
                   'value' : result.stdout.decode('utf-8'),
                   'sufix' : ''})                   

tempstr = prettify(top)

if os.path.isfile(METASO_STATE_XML):
    f = open(METASO_STATE_XML, "r")
    tempstr += f.read()
    f.close()
print(tempstr)
