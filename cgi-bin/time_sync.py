#!/usr/bin/env python3
import site
import cgi, cgitb
import sys
import os 
import datetime
import time

cgitb.enable()

DATE_FILE_PATH="/tmp/date_val"

print("Content-type: text/plain; charset=iso-8859-1\n")

form = cgi.FieldStorage()
sync_time_val='{:02}:{:02}:{:02} {:04}{:02}{:02}'.format(int(form["hour"].value), 
                                                        int(form["min"].value), 
                                                        int(form["sec"].value), 
                                                        int(form["year"].value), 
                                                        int(form["month"].value)+1, 
                                                        int(form["day"].value))

f=open(DATE_FILE_PATH, 'w')
f.write(sync_time_val)
f.close()
time.sleep(10)
print(datetime.datetime.now())
